<?php

namespace mobileassetsolutions\taxcloud\soap;

class ResponseBase
{

    /**
     * @var MessageType $ResponseType
     * @access public
     */
    public $ResponseType = null;

    /**
     * @var ResponseMessage[] $Messages
     * @access public
     */
    public $Messages = null;

    /**
     * @param MessageType $ResponseType
     * @param ResponseMessage[] $Messages
     * @access public
     */
    public function __construct($ResponseType, $Messages)
    {
      $this->ResponseType = $ResponseType;
      $this->Messages = $Messages;
    }

}
