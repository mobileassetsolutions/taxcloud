<?php

namespace mobileassetsolutions\taxcloud\soap;

class Ping
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @access public
     */
    public function __construct($apiLoginID, $apiKey)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
    }

}
