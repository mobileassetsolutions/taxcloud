<?php

namespace mobileassetsolutions\taxcloud\soap;

class ReturnedResponse
{

    /**
     * @var ReturnedRsp $ReturnedResult
     * @access public
     */
    public $ReturnedResult = null;

    /**
     * @param ReturnedRsp $ReturnedResult
     * @access public
     */
    public function __construct($ReturnedResult)
    {
      $this->ReturnedResult = $ReturnedResult;
    }

}
