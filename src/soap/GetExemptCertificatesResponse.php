<?php

namespace mobileassetsolutions\taxcloud\soap;

class GetExemptCertificatesResponse
{

    /**
     * @var GetCertificatesRsp $GetExemptCertificatesResult
     * @access public
     */
    public $GetExemptCertificatesResult = null;

    /**
     * @param GetCertificatesRsp $GetExemptCertificatesResult
     * @access public
     */
    public function __construct($GetExemptCertificatesResult)
    {
      $this->GetExemptCertificatesResult = $GetExemptCertificatesResult;
    }

}
