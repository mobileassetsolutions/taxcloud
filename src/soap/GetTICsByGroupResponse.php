<?php

namespace mobileassetsolutions\taxcloud\soap;

class GetTICsByGroupResponse
{

    /**
     * @var GetTICsRsp $GetTICsByGroupResult
     * @access public
     */
    public $GetTICsByGroupResult = null;

    /**
     * @param GetTICsRsp $GetTICsByGroupResult
     * @access public
     */
    public function __construct($GetTICsByGroupResult)
    {
      $this->GetTICsByGroupResult = $GetTICsByGroupResult;
    }

}
