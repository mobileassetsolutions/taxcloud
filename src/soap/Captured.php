<?php

namespace mobileassetsolutions\taxcloud\soap;

class Captured
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var string $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param string $orderID
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $orderID)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->orderID = $orderID;
    }

}
