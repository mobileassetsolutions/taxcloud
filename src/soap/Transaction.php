<?php

namespace mobileassetsolutions\taxcloud\soap;

class Transaction
{

    /**
     * @var string $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $cartID
     * @access public
     */
    public $cartID = null;

    /**
     * @var string $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var TransactionCartItem[] $cartItems
     * @access public
     */
    public $cartItems = null;

    /**
     * @var Address $origin
     * @access public
     */
    public $origin = null;

    /**
     * @var Address $destination
     * @access public
     */
    public $destination = null;

    /**
     * @var boolean $deliveredBySeller
     * @access public
     */
    public $deliveredBySeller = null;

    /**
     * @var ExemptionCertificate $exemptCert
     * @access public
     */
    public $exemptCert = null;

    /**
     * @var dateTime $dateTransaction
     * @access public
     */
    public $dateTransaction = null;

    /**
     * @var dateTime $dateAuthorized
     * @access public
     */
    public $dateAuthorized = null;

    /**
     * @var dateTime $dateCaptured
     * @access public
     */
    public $dateCaptured = null;

    /**
     * @param string $customerID
     * @param string $cartID
     * @param string $orderID
     * @param TransactionCartItem[] $cartItems
     * @param Address $origin
     * @param Address $destination
     * @param boolean $deliveredBySeller
     * @param ExemptionCertificate $exemptCert
     * @param dateTime $dateTransaction
     * @param dateTime $dateAuthorized
     * @param dateTime $dateCaptured
     * @access public
     */
    public function __construct($customerID, $cartID, $orderID, $cartItems, $origin, $destination, $deliveredBySeller, $exemptCert, $dateTransaction, $dateAuthorized, $dateCaptured)
    {
      $this->customerID = $customerID;
      $this->cartID = $cartID;
      $this->orderID = $orderID;
      $this->cartItems = $cartItems;
      $this->origin = $origin;
      $this->destination = $destination;
      $this->deliveredBySeller = $deliveredBySeller;
      $this->exemptCert = $exemptCert;
      $this->dateTransaction = $dateTransaction;
      $this->dateAuthorized = $dateAuthorized;
      $this->dateCaptured = $dateCaptured;
    }

}
