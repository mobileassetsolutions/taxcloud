<?php

namespace mobileassetsolutions\taxcloud\soap;

class MessageType
{
    const __default = 'Error';
    const Error = 'Error';
    const Warning = 'Warning';
    const Informational = 'Informational';
    const OK = 'OK';


}
