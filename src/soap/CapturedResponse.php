<?php

namespace mobileassetsolutions\taxcloud\soap;

class CapturedResponse
{

    /**
     * @var CapturedRsp $CapturedResult
     * @access public
     */
    public $CapturedResult = null;

    /**
     * @param CapturedRsp $CapturedResult
     * @access public
     */
    public function __construct($CapturedResult)
    {
      $this->CapturedResult = $CapturedResult;
    }

}
