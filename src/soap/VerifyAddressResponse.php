<?php

namespace mobileassetsolutions\taxcloud\soap;

class VerifyAddressResponse
{

    /**
     * @var VerifiedAddress $VerifyAddressResult
     * @access public
     */
    public $VerifyAddressResult = null;

    /**
     * @param VerifiedAddress $VerifyAddressResult
     * @access public
     */
    public function __construct($VerifyAddressResult)
    {
      $this->VerifyAddressResult = $VerifyAddressResult;
    }

}
