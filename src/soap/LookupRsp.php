<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('ResponseBase.php');

class LookupRsp extends ResponseBase
{

    /**
     * @var string $CartID
     * @access public
     */
    public $CartID = null;

    /**
     * @var CartItemResponse[] $CartItemsResponse
     * @access public
     */
    public $CartItemsResponse = null;

    /**
     * @param MessageType $ResponseType
     * @param ResponseMessage[] $Messages
     * @param string $CartID
     * @param CartItemResponse[] $CartItemsResponse
     * @access public
     */
    public function __construct($ResponseType, $Messages, $CartID, $CartItemsResponse)
    {
      parent::__construct($ResponseType, $Messages);
      $this->CartID = $CartID;
      $this->CartItemsResponse = $CartItemsResponse;
    }

}
