<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('ResponseBase.php');

class AddCertificateRsp extends ResponseBase
{

    /**
     * @var string $CertificateID
     * @access public
     */
    public $CertificateID = null;

    /**
     * @param MessageType $ResponseType
     * @param ResponseMessage[] $Messages
     * @param string $CertificateID
     * @access public
     */
    public function __construct($ResponseType, $Messages, $CertificateID)
    {
      parent::__construct($ResponseType, $Messages);
      $this->CertificateID = $CertificateID;
    }

}
