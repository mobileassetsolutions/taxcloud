<?php

namespace mobileassetsolutions\taxcloud\soap;

class AddTransactionsResponse
{

    /**
     * @var AddTransactionsRsp $AddTransactionsResult
     * @access public
     */
    public $AddTransactionsResult = null;

    /**
     * @param AddTransactionsRsp $AddTransactionsResult
     * @access public
     */
    public function __construct($AddTransactionsResult)
    {
      $this->AddTransactionsResult = $AddTransactionsResult;
    }

}
