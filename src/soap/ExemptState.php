<?php

namespace mobileassetsolutions\taxcloud\soap;

class ExemptState
{

    /**
     * @var State $StateAbbr
     * @access public
     */
    public $StateAbbr = null;

    /**
     * @var string $ReasonForExemption
     * @access public
     */
    public $ReasonForExemption = null;

    /**
     * @var string $IdentificationNumber
     * @access public
     */
    public $IdentificationNumber = null;

    /**
     * @param State $StateAbbr
     * @param string $ReasonForExemption
     * @param string $IdentificationNumber
     * @access public
     */
    public function __construct($StateAbbr, $ReasonForExemption, $IdentificationNumber)
    {
      $this->StateAbbr = $StateAbbr;
      $this->ReasonForExemption = $ReasonForExemption;
      $this->IdentificationNumber = $IdentificationNumber;
    }

}
