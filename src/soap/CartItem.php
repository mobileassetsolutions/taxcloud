<?php

namespace mobileassetsolutions\taxcloud\soap;

class CartItem
{

    /**
     * @var int $Index
     * @access public
     */
    public $Index = null;

    /**
     * @var string $ItemID
     * @access public
     */
    public $ItemID = null;

    /**
     * @var int $TIC
     * @access public
     */
    public $TIC = null;

    /**
     * @var float $Price
     * @access public
     */
    public $Price = null;

    /**
     * @var float $Qty
     * @access public
     */
    public $Qty = null;

    /**
     * @param int $Index
     * @param string $ItemID
     * @param float $Price
     * @param float $Qty
     * @access public
     */
    public function __construct($Index, $ItemID, $Price, $Qty)
    {
      $this->Index = $Index;
      $this->ItemID = $ItemID;
      $this->Price = $Price;
      $this->Qty = $Qty;
    }

}
