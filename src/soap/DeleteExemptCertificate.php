<?php

namespace mobileassetsolutions\taxcloud\soap;

class DeleteExemptCertificate
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var string $certificateID
     * @access public
     */
    public $certificateID = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param string $certificateID
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $certificateID)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->certificateID = $certificateID;
    }

}
