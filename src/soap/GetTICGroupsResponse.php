<?php

namespace mobileassetsolutions\taxcloud\soap;

class GetTICGroupsResponse
{

    /**
     * @var GetTICGroupsRsp $GetTICGroupsResult
     * @access public
     */
    public $GetTICGroupsResult = null;

    /**
     * @param GetTICGroupsRsp $GetTICGroupsResult
     * @access public
     */
    public function __construct($GetTICGroupsResult)
    {
      $this->GetTICGroupsResult = $GetTICGroupsResult;
    }

}
