<?php

namespace mobileassetsolutions\taxcloud\soap;

class Returned
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var string $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var CartItem[] $cartItems
     * @access public
     */
    public $cartItems = null;

    /**
     * @var dateTime $returnedDate
     * @access public
     */
    public $returnedDate = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param string $orderID
     * @param CartItem[] $cartItems
     * @param dateTime $returnedDate
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $orderID, $cartItems, $returnedDate)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->orderID = $orderID;
      $this->cartItems = $cartItems;
      $this->returnedDate = $returnedDate;
    }

}
