<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('ResponseBase.php');

class GetTICsRsp extends ResponseBase
{

    /**
     * @var TIC[] $TICs
     * @access public
     */
    public $TICs = null;

    /**
     * @param MessageType $ResponseType
     * @param ResponseMessage[] $Messages
     * @param TIC[] $TICs
     * @access public
     */
    public function __construct($ResponseType, $Messages, $TICs)
    {
      parent::__construct($ResponseType, $Messages);
      $this->TICs = $TICs;
    }

}
