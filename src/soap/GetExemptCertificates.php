<?php

namespace mobileassetsolutions\taxcloud\soap;

class GetExemptCertificates
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var string $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param string $customerID
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $customerID)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->customerID = $customerID;
    }

}
