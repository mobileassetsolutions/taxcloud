<?php

namespace mobileassetsolutions\taxcloud\soap;

class ExemptionCertificateDetail
{

    /**
     * @var ExemptState[] $ExemptStates
     * @access public
     */
    public $ExemptStates = null;

    /**
     * @var boolean $SinglePurchase
     * @access public
     */
    public $SinglePurchase = null;

    /**
     * @var string $SinglePurchaseOrderNumber
     * @access public
     */
    public $SinglePurchaseOrderNumber = null;

    /**
     * @var string $PurchaserFirstName
     * @access public
     */
    public $PurchaserFirstName = null;

    /**
     * @var string $PurchaserLastName
     * @access public
     */
    public $PurchaserLastName = null;

    /**
     * @var string $PurchaserTitle
     * @access public
     */
    public $PurchaserTitle = null;

    /**
     * @var string $PurchaserAddress1
     * @access public
     */
    public $PurchaserAddress1 = null;

    /**
     * @var string $PurchaserAddress2
     * @access public
     */
    public $PurchaserAddress2 = null;

    /**
     * @var string $PurchaserCity
     * @access public
     */
    public $PurchaserCity = null;

    /**
     * @var State $PurchaserState
     * @access public
     */
    public $PurchaserState = null;

    /**
     * @var string $PurchaserZip
     * @access public
     */
    public $PurchaserZip = null;

    /**
     * @var TaxID $PurchaserTaxID
     * @access public
     */
    public $PurchaserTaxID = null;

    /**
     * @var BusinessType $PurchaserBusinessType
     * @access public
     */
    public $PurchaserBusinessType = null;

    /**
     * @var string $PurchaserBusinessTypeOtherValue
     * @access public
     */
    public $PurchaserBusinessTypeOtherValue = null;

    /**
     * @var ExemptionReason $PurchaserExemptionReason
     * @access public
     */
    public $PurchaserExemptionReason = null;

    /**
     * @var string $PurchaserExemptionReasonValue
     * @access public
     */
    public $PurchaserExemptionReasonValue = null;

    /**
     * @var dateTime $CreatedDate
     * @access public
     */
    public $CreatedDate = null;

    /**
     * @param ExemptState[] $ExemptStates
     * @param boolean $SinglePurchase
     * @param string $SinglePurchaseOrderNumber
     * @param string $PurchaserFirstName
     * @param string $PurchaserLastName
     * @param string $PurchaserTitle
     * @param string $PurchaserAddress1
     * @param string $PurchaserAddress2
     * @param string $PurchaserCity
     * @param State $PurchaserState
     * @param string $PurchaserZip
     * @param TaxID $PurchaserTaxID
     * @param BusinessType $PurchaserBusinessType
     * @param string $PurchaserBusinessTypeOtherValue
     * @param ExemptionReason $PurchaserExemptionReason
     * @param string $PurchaserExemptionReasonValue
     * @param dateTime $CreatedDate
     * @access public
     */
    public function __construct($ExemptStates, $SinglePurchase, $SinglePurchaseOrderNumber, $PurchaserFirstName, $PurchaserLastName, $PurchaserTitle, $PurchaserAddress1, $PurchaserAddress2, $PurchaserCity, $PurchaserState, $PurchaserZip, $PurchaserTaxID, $PurchaserBusinessType, $PurchaserBusinessTypeOtherValue, $PurchaserExemptionReason, $PurchaserExemptionReasonValue, $CreatedDate)
    {
      $this->ExemptStates = $ExemptStates;
      $this->SinglePurchase = $SinglePurchase;
      $this->SinglePurchaseOrderNumber = $SinglePurchaseOrderNumber;
      $this->PurchaserFirstName = $PurchaserFirstName;
      $this->PurchaserLastName = $PurchaserLastName;
      $this->PurchaserTitle = $PurchaserTitle;
      $this->PurchaserAddress1 = $PurchaserAddress1;
      $this->PurchaserAddress2 = $PurchaserAddress2;
      $this->PurchaserCity = $PurchaserCity;
      $this->PurchaserState = $PurchaserState;
      $this->PurchaserZip = $PurchaserZip;
      $this->PurchaserTaxID = $PurchaserTaxID;
      $this->PurchaserBusinessType = $PurchaserBusinessType;
      $this->PurchaserBusinessTypeOtherValue = $PurchaserBusinessTypeOtherValue;
      $this->PurchaserExemptionReason = $PurchaserExemptionReason;
      $this->PurchaserExemptionReasonValue = $PurchaserExemptionReasonValue;
      $this->CreatedDate = $CreatedDate;
    }

}
