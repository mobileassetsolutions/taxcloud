<?php

namespace mobileassetsolutions\taxcloud\soap;

class AuthorizedResponse
{

    /**
     * @var AuthorizedRsp $AuthorizedResult
     * @access public
     */
    public $AuthorizedResult = null;

    /**
     * @param AuthorizedRsp $AuthorizedResult
     * @access public
     */
    public function __construct($AuthorizedResult)
    {
      $this->AuthorizedResult = $AuthorizedResult;
    }

}
