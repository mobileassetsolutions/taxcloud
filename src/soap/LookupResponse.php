<?php

namespace mobileassetsolutions\taxcloud\soap;

class LookupResponse
{

    /**
     * @var LookupRsp $LookupResult
     * @access public
     */
    public $LookupResult = null;

    /**
     * @param LookupRsp $LookupResult
     * @access public
     */
    public function __construct($LookupResult)
    {
      $this->LookupResult = $LookupResult;
    }

}
