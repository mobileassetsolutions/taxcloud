<?php

namespace mobileassetsolutions\taxcloud\soap;

class CartItemResponse
{

    /**
     * @var int $CartItemIndex
     * @access public
     */
    public $CartItemIndex = null;

    /**
     * @var float $TaxAmount
     * @access public
     */
    public $TaxAmount = null;

    /**
     * @param int $CartItemIndex
     * @param float $TaxAmount
     * @access public
     */
    public function __construct($CartItemIndex, $TaxAmount)
    {
      $this->CartItemIndex = $CartItemIndex;
      $this->TaxAmount = $TaxAmount;
    }

}
