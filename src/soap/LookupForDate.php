<?php

namespace mobileassetsolutions\taxcloud\soap;

class LookupForDate
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var string $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $cartID
     * @access public
     */
    public $cartID = null;

    /**
     * @var CartItem[] $cartItems
     * @access public
     */
    public $cartItems = null;

    /**
     * @var Address $origin
     * @access public
     */
    public $origin = null;

    /**
     * @var Address $destination
     * @access public
     */
    public $destination = null;

    /**
     * @var boolean $deliveredBySeller
     * @access public
     */
    public $deliveredBySeller = null;

    /**
     * @var ExemptionCertificate $exemptCert
     * @access public
     */
    public $exemptCert = null;

    /**
     * @var dateTime $useDate
     * @access public
     */
    public $useDate = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param string $customerID
     * @param string $cartID
     * @param CartItem[] $cartItems
     * @param Address $origin
     * @param Address $destination
     * @param boolean $deliveredBySeller
     * @param ExemptionCertificate $exemptCert
     * @param dateTime $useDate
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $customerID, $cartID, $cartItems, $origin, $destination, $deliveredBySeller, $exemptCert, $useDate)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->customerID = $customerID;
      $this->cartID = $cartID;
      $this->cartItems = $cartItems;
      $this->origin = $origin;
      $this->destination = $destination;
      $this->deliveredBySeller = $deliveredBySeller;
      $this->exemptCert = $exemptCert;
      $this->useDate = $useDate;
    }

}
