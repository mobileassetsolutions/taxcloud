<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('ResponseBase.php');

class GetCertificatesRsp extends ResponseBase
{

    /**
     * @var ExemptionCertificate[] $ExemptCertificates
     * @access public
     */
    public $ExemptCertificates = null;

    /**
     * @param MessageType $ResponseType
     * @param ResponseMessage[] $Messages
     * @param ExemptionCertificate[] $ExemptCertificates
     * @access public
     */
    public function __construct($ResponseType, $Messages, $ExemptCertificates)
    {
      parent::__construct($ResponseType, $Messages);
      $this->ExemptCertificates = $ExemptCertificates;
    }

}
