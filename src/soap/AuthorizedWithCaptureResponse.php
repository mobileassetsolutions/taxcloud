<?php

namespace mobileassetsolutions\taxcloud\soap;

class AuthorizedWithCaptureResponse
{

    /**
     * @var AuthorizedRsp $AuthorizedWithCaptureResult
     * @access public
     */
    public $AuthorizedWithCaptureResult = null;

    /**
     * @param AuthorizedRsp $AuthorizedWithCaptureResult
     * @access public
     */
    public function __construct($AuthorizedWithCaptureResult)
    {
      $this->AuthorizedWithCaptureResult = $AuthorizedWithCaptureResult;
    }

}
