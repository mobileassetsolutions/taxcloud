<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('ResponseBase.php');

class GetTICGroupsRsp extends ResponseBase
{

    /**
     * @var TICGroup[] $TICGroups
     * @access public
     */
    public $TICGroups = null;

    /**
     * @param MessageType $ResponseType
     * @param ResponseMessage[] $Messages
     * @param TICGroup[] $TICGroups
     * @access public
     */
    public function __construct($ResponseType, $Messages, $TICGroups)
    {
      parent::__construct($ResponseType, $Messages);
      $this->TICGroups = $TICGroups;
    }

}
