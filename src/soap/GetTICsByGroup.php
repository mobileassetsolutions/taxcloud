<?php

namespace mobileassetsolutions\taxcloud\soap;

class GetTICsByGroup
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var int $ticGroup
     * @access public
     */
    public $ticGroup = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param int $ticGroup
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $ticGroup)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->ticGroup = $ticGroup;
    }

}
