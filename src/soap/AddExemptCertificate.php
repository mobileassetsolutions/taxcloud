<?php

namespace mobileassetsolutions\taxcloud\soap;

class AddExemptCertificate
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var string $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var ExemptionCertificate $exemptCert
     * @access public
     */
    public $exemptCert = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param string $customerID
     * @param ExemptionCertificate $exemptCert
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $customerID, $exemptCert)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->customerID = $customerID;
      $this->exemptCert = $exemptCert;
    }

}
