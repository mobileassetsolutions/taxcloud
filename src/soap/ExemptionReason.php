<?php

namespace mobileassetsolutions\taxcloud\soap;

class ExemptionReason
{
    const __default = 'FederalGovernmentDepartment';
    const FederalGovernmentDepartment = 'FederalGovernmentDepartment';
    const StateOrLocalGovernmentName = 'StateOrLocalGovernmentName';
    const TribalGovernmentName = 'TribalGovernmentName';
    const ForeignDiplomat = 'ForeignDiplomat';
    const CharitableOrganization = 'CharitableOrganization';
    const ReligiousOrEducationalOrganization = 'ReligiousOrEducationalOrganization';
    const Resale = 'Resale';
    const AgriculturalProduction = 'AgriculturalProduction';
    const IndustrialProductionOrManufacturing = 'IndustrialProductionOrManufacturing';
    const DirectPayPermit = 'DirectPayPermit';
    const DirectMail = 'DirectMail';
    const Other = 'Other';


}
