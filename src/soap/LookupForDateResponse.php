<?php

namespace mobileassetsolutions\taxcloud\soap;

class LookupForDateResponse
{

    /**
     * @var LookupRsp $LookupForDateResult
     * @access public
     */
    public $LookupForDateResult = null;

    /**
     * @param LookupRsp $LookupForDateResult
     * @access public
     */
    public function __construct($LookupForDateResult)
    {
      $this->LookupForDateResult = $LookupForDateResult;
    }

}
