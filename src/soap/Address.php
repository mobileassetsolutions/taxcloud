<?php

namespace mobileassetsolutions\taxcloud\soap;

class Address
{

    /**
     * @var string $Address1
     * @access public
     */
    public $Address1 = null;

    /**
     * @var string $Address2
     * @access public
     */
    public $Address2 = null;

    /**
     * @var string $City
     * @access public
     */
    public $City = null;

    /**
     * @var string $State
     * @access public
     */
    public $State = null;

    /**
     * @var string $Zip5
     * @access public
     */
    public $Zip5 = null;

    /**
     * @var string $Zip4
     * @access public
     */
    public $Zip4 = null;

    /**
     * @param string $Address1
     * @param string $Address2
     * @param string $City
     * @param string $State
     * @param string $Zip5
     * @param string $Zip4
     * @access public
     */
    public function __construct($Address1, $Address2, $City, $State, $Zip5, $Zip4)
    {
      $this->Address1 = $Address1;
      $this->Address2 = $Address2;
      $this->City = $City;
      $this->State = $State;
      $this->Zip5 = $Zip5;
      $this->Zip4 = $Zip4;
    }

}
