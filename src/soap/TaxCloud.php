<?php

namespace mobileassetsolutions\taxcloud\soap;

/**
 * TaxCloud Web Service
 */
class TaxCloud extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'VerifyAddress' => 'mobileassetsolutions\taxcloud\soap\VerifyAddress',
      'VerifyAddressResponse' => 'mobileassetsolutions\taxcloud\soap\VerifyAddressResponse',
      'VerifiedAddress' => 'mobileassetsolutions\taxcloud\soap\VerifiedAddress',
      'Address' => 'mobileassetsolutions\taxcloud\soap\Address',
      'LookupForDate' => 'mobileassetsolutions\taxcloud\soap\LookupForDate',
      'CartItem' => 'mobileassetsolutions\taxcloud\soap\CartItem',
      'ExemptionCertificate' => 'mobileassetsolutions\taxcloud\soap\ExemptionCertificate',
      'ExemptionCertificateDetail' => 'mobileassetsolutions\taxcloud\soap\ExemptionCertificateDetail',
      'ExemptState' => 'mobileassetsolutions\taxcloud\soap\ExemptState',
      'TaxID' => 'mobileassetsolutions\taxcloud\soap\TaxID',
      'LookupForDateResponse' => 'mobileassetsolutions\taxcloud\soap\LookupForDateResponse',
      'LookupRsp' => 'mobileassetsolutions\taxcloud\soap\LookupRsp',
      'ResponseBase' => 'mobileassetsolutions\taxcloud\soap\ResponseBase',
      'ResponseMessage' => 'mobileassetsolutions\taxcloud\soap\ResponseMessage',
      'CartItemResponse' => 'mobileassetsolutions\taxcloud\soap\CartItemResponse',
      'Lookup' => 'mobileassetsolutions\taxcloud\soap\Lookup',
      'LookupResponse' => 'mobileassetsolutions\taxcloud\soap\LookupResponse',
      'Authorized' => 'mobileassetsolutions\taxcloud\soap\Authorized',
      'AuthorizedResponse' => 'mobileassetsolutions\taxcloud\soap\AuthorizedResponse',
      'AuthorizedRsp' => 'mobileassetsolutions\taxcloud\soap\AuthorizedRsp',
      'AuthorizedWithCapture' => 'mobileassetsolutions\taxcloud\soap\AuthorizedWithCapture',
      'AuthorizedWithCaptureResponse' => 'mobileassetsolutions\taxcloud\soap\AuthorizedWithCaptureResponse',
      'Captured' => 'mobileassetsolutions\taxcloud\soap\Captured',
      'CapturedResponse' => 'mobileassetsolutions\taxcloud\soap\CapturedResponse',
      'CapturedRsp' => 'mobileassetsolutions\taxcloud\soap\CapturedRsp',
      'Returned' => 'mobileassetsolutions\taxcloud\soap\Returned',
      'ReturnedResponse' => 'mobileassetsolutions\taxcloud\soap\ReturnedResponse',
      'ReturnedRsp' => 'mobileassetsolutions\taxcloud\soap\ReturnedRsp',
      'AddTransactions' => 'mobileassetsolutions\taxcloud\soap\AddTransactions',
      'Transaction' => 'mobileassetsolutions\taxcloud\soap\Transaction',
      'TransactionCartItem' => 'mobileassetsolutions\taxcloud\soap\TransactionCartItem',
      'AddTransactionsResponse' => 'mobileassetsolutions\taxcloud\soap\AddTransactionsResponse',
      'AddTransactionsRsp' => 'mobileassetsolutions\taxcloud\soap\AddTransactionsRsp',
      'GetTICGroups' => 'mobileassetsolutions\taxcloud\soap\GetTICGroups',
      'GetTICGroupsResponse' => 'mobileassetsolutions\taxcloud\soap\GetTICGroupsResponse',
      'GetTICGroupsRsp' => 'mobileassetsolutions\taxcloud\soap\GetTICGroupsRsp',
      'TICGroup' => 'mobileassetsolutions\taxcloud\soap\TICGroup',
      'GetTICs' => 'mobileassetsolutions\taxcloud\soap\GetTICs',
      'GetTICsResponse' => 'mobileassetsolutions\taxcloud\soap\GetTICsResponse',
      'GetTICsRsp' => 'mobileassetsolutions\taxcloud\soap\GetTICsRsp',
      'TIC' => 'mobileassetsolutions\taxcloud\soap\TIC',
      'GetTICsByGroup' => 'mobileassetsolutions\taxcloud\soap\GetTICsByGroup',
      'GetTICsByGroupResponse' => 'mobileassetsolutions\taxcloud\soap\GetTICsByGroupResponse',
      'AddExemptCertificate' => 'mobileassetsolutions\taxcloud\soap\AddExemptCertificate',
      'AddExemptCertificateResponse' => 'mobileassetsolutions\taxcloud\soap\AddExemptCertificateResponse',
      'AddCertificateRsp' => 'mobileassetsolutions\taxcloud\soap\AddCertificateRsp',
      'DeleteExemptCertificate' => 'mobileassetsolutions\taxcloud\soap\DeleteExemptCertificate',
      'DeleteExemptCertificateResponse' => 'mobileassetsolutions\taxcloud\soap\DeleteExemptCertificateResponse',
      'DeleteCertificateRsp' => 'mobileassetsolutions\taxcloud\soap\DeleteCertificateRsp',
      'GetExemptCertificates' => 'mobileassetsolutions\taxcloud\soap\GetExemptCertificates',
      'GetExemptCertificatesResponse' => 'mobileassetsolutions\taxcloud\soap\GetExemptCertificatesResponse',
      'GetCertificatesRsp' => 'mobileassetsolutions\taxcloud\soap\GetCertificatesRsp',
      'Ping' => 'mobileassetsolutions\taxcloud\soap\Ping',
      'PingResponse' => 'mobileassetsolutions\taxcloud\soap\PingResponse',
      'PingRsp' => 'mobileassetsolutions\taxcloud\soap\PingRsp');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'https://api.taxcloud.net/1.0/?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param VerifyAddress $parameters
     * @access public
     * @return VerifyAddressResponse
     */
    public function VerifyAddress(VerifyAddress $parameters)
    {
      return $this->__soapCall('VerifyAddress', array($parameters));
    }

    /**
     * @param LookupForDate $parameters
     * @access public
     * @return LookupForDateResponse
     */
    public function LookupForDate(LookupForDate $parameters)
    {
      return $this->__soapCall('LookupForDate', array($parameters));
    }

    /**
     * @param Lookup $parameters
     * @access public
     * @return LookupResponse
     */
    public function Lookup(Lookup $parameters)
    {
      return $this->__soapCall('Lookup', array($parameters));
    }

    /**
     * @param Authorized $parameters
     * @access public
     * @return AuthorizedResponse
     */
    public function Authorized(Authorized $parameters)
    {
      return $this->__soapCall('Authorized', array($parameters));
    }

    /**
     * @param AuthorizedWithCapture $parameters
     * @access public
     * @return AuthorizedWithCaptureResponse
     */
    public function AuthorizedWithCapture(AuthorizedWithCapture $parameters)
    {
      return $this->__soapCall('AuthorizedWithCapture', array($parameters));
    }

    /**
     * @param Captured $parameters
     * @access public
     * @return CapturedResponse
     */
    public function Captured(Captured $parameters)
    {
      return $this->__soapCall('Captured', array($parameters));
    }

    /**
     * @param Returned $parameters
     * @access public
     * @return ReturnedResponse
     */
    public function Returned(Returned $parameters)
    {
      return $this->__soapCall('Returned', array($parameters));
    }

    /**
     * @param AddTransactions $parameters
     * @access public
     * @return AddTransactionsResponse
     */
    public function AddTransactions(AddTransactions $parameters)
    {
      return $this->__soapCall('AddTransactions', array($parameters));
    }

    /**
     * @param GetTICGroups $parameters
     * @access public
     * @return GetTICGroupsResponse
     */
    public function GetTICGroups(GetTICGroups $parameters)
    {
      return $this->__soapCall('GetTICGroups', array($parameters));
    }

    /**
     * @param GetTICs $parameters
     * @access public
     * @return GetTICsResponse
     */
    public function GetTICs(GetTICs $parameters)
    {
      return $this->__soapCall('GetTICs', array($parameters));
    }

    /**
     * @param GetTICsByGroup $parameters
     * @access public
     * @return GetTICsByGroupResponse
     */
    public function GetTICsByGroup(GetTICsByGroup $parameters)
    {
      return $this->__soapCall('GetTICsByGroup', array($parameters));
    }

    /**
     * @param AddExemptCertificate $parameters
     * @access public
     * @return AddExemptCertificateResponse
     */
    public function AddExemptCertificate(AddExemptCertificate $parameters)
    {
      return $this->__soapCall('AddExemptCertificate', array($parameters));
    }

    /**
     * @param DeleteExemptCertificate $parameters
     * @access public
     * @return DeleteExemptCertificateResponse
     */
    public function DeleteExemptCertificate(DeleteExemptCertificate $parameters)
    {
      return $this->__soapCall('DeleteExemptCertificate', array($parameters));
    }

    /**
     * @param GetExemptCertificates $parameters
     * @access public
     * @return GetExemptCertificatesResponse
     */
    public function GetExemptCertificates(GetExemptCertificates $parameters)
    {
      return $this->__soapCall('GetExemptCertificates', array($parameters));
    }

    /**
     * @param Ping $parameters
     * @access public
     * @return PingResponse
     */
    public function Ping(Ping $parameters)
    {
      return $this->__soapCall('Ping', array($parameters));
    }

}
