<?php

namespace mobileassetsolutions\taxcloud\soap;

class PingResponse
{

    /**
     * @var PingRsp $PingResult
     * @access public
     */
    public $PingResult = null;

    /**
     * @param PingRsp $PingResult
     * @access public
     */
    public function __construct($PingResult)
    {
      $this->PingResult = $PingResult;
    }

}
