<?php

namespace mobileassetsolutions\taxcloud\soap;

class ExemptionCertificate
{

    /**
     * @var string $CertificateID
     * @access public
     */
    public $CertificateID = null;

    /**
     * @var ExemptionCertificateDetail $Detail
     * @access public
     */
    public $Detail = null;

    /**
     * @param string $CertificateID
     * @param ExemptionCertificateDetail $Detail
     * @access public
     */
    public function __construct($CertificateID, $Detail)
    {
      $this->CertificateID = $CertificateID;
      $this->Detail = $Detail;
    }

}
