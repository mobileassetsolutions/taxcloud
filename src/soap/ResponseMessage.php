<?php

namespace mobileassetsolutions\taxcloud\soap;

class ResponseMessage
{

    /**
     * @var MessageType $ResponseType
     * @access public
     */
    public $ResponseType = null;

    /**
     * @var string $Message
     * @access public
     */
    public $Message = null;

    /**
     * @param MessageType $ResponseType
     * @param string $Message
     * @access public
     */
    public function __construct($ResponseType, $Message)
    {
      $this->ResponseType = $ResponseType;
      $this->Message = $Message;
    }

}
