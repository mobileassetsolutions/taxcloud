<?php

namespace mobileassetsolutions\taxcloud\soap;

class BusinessType
{
    const __default = 'AccommodationAndFoodServices';
    const AccommodationAndFoodServices = 'AccommodationAndFoodServices';
    const Agricultural_Forestry_Fishing_Hunting = 'Agricultural_Forestry_Fishing_Hunting';
    const Construction = 'Construction';
    const FinanceAndInsurance = 'FinanceAndInsurance';
    const Information_PublishingAndCommunications = 'Information_PublishingAndCommunications';
    const Manufacturing = 'Manufacturing';
    const Mining = 'Mining';
    const RealEstate = 'RealEstate';
    const RentalAndLeasing = 'RentalAndLeasing';
    const RetailTrade = 'RetailTrade';
    const TransportationAndWarehousing = 'TransportationAndWarehousing';
    const Utilities = 'Utilities';
    const WholesaleTrade = 'WholesaleTrade';
    const BusinessServices = 'BusinessServices';
    const ProfessionalServices = 'ProfessionalServices';
    const EducationAndHealthCareServices = 'EducationAndHealthCareServices';
    const NonprofitOrganization = 'NonprofitOrganization';
    const Government = 'Government';
    const NotABusiness = 'NotABusiness';
    const Other = 'Other';


}
