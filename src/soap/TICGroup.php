<?php

namespace mobileassetsolutions\taxcloud\soap;

class TICGroup
{

    /**
     * @var int $GroupID
     * @access public
     */
    public $GroupID = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @param int $GroupID
     * @param string $Description
     * @access public
     */
    public function __construct($GroupID, $Description)
    {
      $this->GroupID = $GroupID;
      $this->Description = $Description;
    }

}
