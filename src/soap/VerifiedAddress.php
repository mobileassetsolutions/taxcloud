<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('Address.php');

class VerifiedAddress extends Address
{

    /**
     * @var string $ErrNumber
     * @access public
     */
    public $ErrNumber = null;

    /**
     * @var string $ErrDescription
     * @access public
     */
    public $ErrDescription = null;

    /**
     * @param string $Address1
     * @param string $Address2
     * @param string $City
     * @param string $State
     * @param string $Zip5
     * @param string $Zip4
     * @param string $ErrNumber
     * @param string $ErrDescription
     * @access public
     */
    public function __construct($Address1, $Address2, $City, $State, $Zip5, $Zip4, $ErrNumber, $ErrDescription)
    {
      parent::__construct($Address1, $Address2, $City, $State, $Zip5, $Zip4);
      $this->ErrNumber = $ErrNumber;
      $this->ErrDescription = $ErrDescription;
    }

}
