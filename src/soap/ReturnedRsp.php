<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('ResponseBase.php');

class ReturnedRsp extends ResponseBase
{

    /**
     * @param MessageType $ResponseType
     * @param ResponseMessage[] $Messages
     * @access public
     */
    public function __construct($ResponseType, $Messages)
    {
      parent::__construct($ResponseType, $Messages);
    }

}
