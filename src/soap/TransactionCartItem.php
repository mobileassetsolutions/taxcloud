<?php

namespace mobileassetsolutions\taxcloud\soap;

include_once('CartItem.php');

class TransactionCartItem extends CartItem
{

    /**
     * @var float $Rate
     * @access public
     */
    public $Rate = null;

    /**
     * @param int $Index
     * @param string $ItemID
     * @param float $Price
     * @param float $Qty
     * @param float $Rate
     * @access public
     */
    public function __construct($Index, $ItemID, $Price, $Qty, $Rate)
    {
      parent::__construct($Index, $ItemID, $Price, $Qty);
      $this->Rate = $Rate;
    }

}
