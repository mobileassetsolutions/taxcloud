<?php

namespace mobileassetsolutions\taxcloud\soap;

class AddTransactions
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var Transaction[] $transactions
     * @access public
     */
    public $transactions = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param Transaction[] $transactions
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $transactions)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->transactions = $transactions;
    }

}
