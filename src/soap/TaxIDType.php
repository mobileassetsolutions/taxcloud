<?php

namespace mobileassetsolutions\taxcloud\soap;

class TaxIDType
{
    const __default = 'SSN';
    const SSN = 'SSN';
    const FEIN = 'FEIN';
    const StateIssued = 'StateIssued';
    const ForeignDiplomat = 'ForeignDiplomat';


}
