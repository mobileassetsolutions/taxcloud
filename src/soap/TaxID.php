<?php

namespace mobileassetsolutions\taxcloud\soap;

class TaxID
{

    /**
     * @var TaxIDType $TaxType
     * @access public
     */
    public $TaxType = null;

    /**
     * @var string $IDNumber
     * @access public
     */
    public $IDNumber = null;

    /**
     * @var string $StateOfIssue
     * @access public
     */
    public $StateOfIssue = null;

    /**
     * @param TaxIDType $TaxType
     * @param string $IDNumber
     * @param string $StateOfIssue
     * @access public
     */
    public function __construct($TaxType, $IDNumber, $StateOfIssue)
    {
      $this->TaxType = $TaxType;
      $this->IDNumber = $IDNumber;
      $this->StateOfIssue = $StateOfIssue;
    }

}
