<?php

namespace mobileassetsolutions\taxcloud\soap;

class TIC
{

    /**
     * @var int $TICID
     * @access public
     */
    public $TICID = null;

    /**
     * @var string $Description
     * @access public
     */
    public $Description = null;

    /**
     * @param int $TICID
     * @param string $Description
     * @access public
     */
    public function __construct($TICID, $Description)
    {
      $this->TICID = $TICID;
      $this->Description = $Description;
    }

}
