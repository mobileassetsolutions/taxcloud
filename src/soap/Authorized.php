<?php

namespace mobileassetsolutions\taxcloud\soap;

class Authorized
{

    /**
     * @var string $apiLoginID
     * @access public
     */
    public $apiLoginID = null;

    /**
     * @var string $apiKey
     * @access public
     */
    public $apiKey = null;

    /**
     * @var string $customerID
     * @access public
     */
    public $customerID = null;

    /**
     * @var string $cartID
     * @access public
     */
    public $cartID = null;

    /**
     * @var string $orderID
     * @access public
     */
    public $orderID = null;

    /**
     * @var dateTime $dateAuthorized
     * @access public
     */
    public $dateAuthorized = null;

    /**
     * @param string $apiLoginID
     * @param string $apiKey
     * @param string $customerID
     * @param string $cartID
     * @param string $orderID
     * @param dateTime $dateAuthorized
     * @access public
     */
    public function __construct($apiLoginID, $apiKey, $customerID, $cartID, $orderID, $dateAuthorized)
    {
      $this->apiLoginID = $apiLoginID;
      $this->apiKey = $apiKey;
      $this->customerID = $customerID;
      $this->cartID = $cartID;
      $this->orderID = $orderID;
      $this->dateAuthorized = $dateAuthorized;
    }

}
