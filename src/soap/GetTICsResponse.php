<?php

namespace mobileassetsolutions\taxcloud\soap;

class GetTICsResponse
{

    /**
     * @var GetTICsRsp $GetTICsResult
     * @access public
     */
    public $GetTICsResult = null;

    /**
     * @param GetTICsRsp $GetTICsResult
     * @access public
     */
    public function __construct($GetTICsResult)
    {
      $this->GetTICsResult = $GetTICsResult;
    }

}
