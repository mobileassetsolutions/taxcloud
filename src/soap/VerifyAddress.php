<?php

namespace mobileassetsolutions\taxcloud\soap;

class VerifyAddress
{

    /**
     * @var string $uspsUserID
     * @access public
     */
    public $uspsUserID = null;

    /**
     * @var string $address1
     * @access public
     */
    public $address1 = null;

    /**
     * @var string $address2
     * @access public
     */
    public $address2 = null;

    /**
     * @var string $city
     * @access public
     */
    public $city = null;

    /**
     * @var string $state
     * @access public
     */
    public $state = null;

    /**
     * @var string $zip5
     * @access public
     */
    public $zip5 = null;

    /**
     * @var string $zip4
     * @access public
     */
    public $zip4 = null;

    /**
     * @param string $uspsUserID
     * @param string $address1
     * @param string $address2
     * @param string $city
     * @param string $state
     * @param string $zip5
     * @param string $zip4
     * @access public
     */
    public function __construct($uspsUserID, $address1, $address2, $city, $state, $zip5, $zip4)
    {
      $this->uspsUserID = $uspsUserID;
      $this->address1 = $address1;
      $this->address2 = $address2;
      $this->city = $city;
      $this->state = $state;
      $this->zip5 = $zip5;
      $this->zip4 = $zip4;
    }

}
