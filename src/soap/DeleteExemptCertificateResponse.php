<?php

namespace mobileassetsolutions\taxcloud\soap;

class DeleteExemptCertificateResponse
{

    /**
     * @var DeleteCertificateRsp $DeleteExemptCertificateResult
     * @access public
     */
    public $DeleteExemptCertificateResult = null;

    /**
     * @param DeleteCertificateRsp $DeleteExemptCertificateResult
     * @access public
     */
    public function __construct($DeleteExemptCertificateResult)
    {
      $this->DeleteExemptCertificateResult = $DeleteExemptCertificateResult;
    }

}
