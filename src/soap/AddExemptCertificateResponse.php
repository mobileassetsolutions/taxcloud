<?php

namespace mobileassetsolutions\taxcloud\soap;

class AddExemptCertificateResponse
{

    /**
     * @var AddCertificateRsp $AddExemptCertificateResult
     * @access public
     */
    public $AddExemptCertificateResult = null;

    /**
     * @param AddCertificateRsp $AddExemptCertificateResult
     * @access public
     */
    public function __construct($AddExemptCertificateResult)
    {
      $this->AddExemptCertificateResult = $AddExemptCertificateResult;
    }

}
