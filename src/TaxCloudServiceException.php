<?php

namespace mobileassetsolutions\taxcloud;

/**
 * Class to use for API errors
 */
class TaxCloudServiceException extends \Exception
{
   protected $response;

   public function __construct(soap\ResponseBase $response = null, \SoapFault $exception = null)
   {
      $this->response = $response;

      if (isset($exception))
      {
         parent::__construct("SoapFault calling API: " . $exception->getMessage(), 0, $exception);
      }
      elseif (isset($response->Messages->ResponseMessage))
      {
         parent::__construct("API returned {$response->Messages->ResponseMessage->ResponseType}: {$response->Messages->ResponseMessage->Message}.");
      }
      else
      {
         parent::__construct("Unknown error see getResponse() for more details.");
      }
   }

   public function getResponse()
   {
      return $this->response;
   }

   public static function throwIfError(soap\ResponseBase $response)
   {

   }
}
