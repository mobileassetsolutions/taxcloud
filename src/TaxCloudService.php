<?php

namespace mobileassetsolutions\taxcloud;

/**
 * Wrapper class around raw TaxCloud SOAP client to wrap errors in our own exception class
 */
class TaxCloudService extends soap\TaxCloud
{
    public function __soapCall($method, $args, $options = null, $input_headers = null, &$output_headers = null)
    {
        try
        {
            $result = parent::__soapCall($method, $args);
            if (isset($result->ResponseType) && $result->ResponseType == 'Error')
            {
                throw new TaxCloudServiceException($result);
            }

            return $result;
        }
        catch (\SoapFault $e)
        {
            throw new TaxCloudServiceException(null, $e);
        }
    }
}
